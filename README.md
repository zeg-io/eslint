# README #

This will create a directory with two subdirectories, an ES6, and an ES6-Prod.  The ES6-Prod is more strict and doesn't allow // TODO: comments

### Why? ###

* Consistent code styles
* Faster reading
* Faster debugging
* Faster brining others up to speed if everyone is used to a consistent style.

### How do I get set up? ###

* Put these files in a folder and point WebStorm (or whatever ESLint enabled tool you'd like to use at the relevant `eslintrc` file

### Contribution guidelines ###

* Talk to Tony

### Who do I talk to? ###

* tony@zeg.io